const express = require('express');
const {
    check, body
} = require('express-validator/check');

const authController = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

router.get('/login', authController.getLogin);
router.get('/signup', authController.getSignup);
router.post(
    '/login',
    [
        body('email')
          .isEmail()
          .withMessage('Please enter a valid email address.')
          .normalizeEmail(),
        body('password', 'Please enter a password which is alphanumeric and has at least 5 characters')
          .isLength({ min: 5 })
          .isAlphanumeric()
          .trim(),
    ],
    authController.postLogin
);
router.post(
    '/signup',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email.')
            .custom((value, {req}) => {
                // if (value === 'test@test.ch') {
                //     throw new Error('This email address is forbidden.');
                // }
                // return true;
                return User
                    .findOne({'email': req.body.email})
                    .then(userDoc => {
                        if (userDoc) {
                            return Promise.reject('E-Mail exists already, please pick a different one.');
                        }
                    });
            })
            .normalizeEmail(),
        body(
            'password',
            'Please enter a password which is alphanumeric and has at least 5 characters'   // default message for all validators
        )
            .isLength({ min: 5 })
            .isAlphanumeric()
            .trim(),
        body('confirmPassword')
            .trim()
            .custom((value, { req }) => {
                return value === req.body.password;
            })
            .withMessage('Passwords have to match'),
    ],
    authController.postSignup
);
router.post('/logout', authController.postLogout);
router.get('/reset', authController.getReset);
router.post('/reset', authController.postReset);
router.get('/reset/:token', authController.getNewPassword);
router.post('/new-password', authController.postNewPassword);

module.exports = router;