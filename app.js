const path = require('path');
const fs = require('fs');
const https = require('https');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');

const errorController = require('./controllers/error');
const User = require('./models/user');
const shopController = require('./controllers/shop');
const isAuth = require('./middleware/is-auth');

const MONGODB_URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-shard-00-00-wlivz.mongodb.net:27017,cluster0-shard-00-01-wlivz.mongodb.net:27017,cluster0-shard-00-02-wlivz.mongodb.net:27017/${process.env.MONGO_DEFAULT_DATABASE}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin`;

const app = express();
const store = new MongoDBStore({    // where the session will be stored
    uri: MONGODB_URI,
    collection: 'sessions',
});
const csrfProtection = csrf();  // use default config

const privateKey = fs.readFileSync('server.key');
const certificate = fs.readFileSync('server.cert');

const fileStorage = multer.diskStorage({    // create a file storage
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + '-' + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ) {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

app.set('view engine', 'ejs'); // set the template engine which will be used with app.render()
app.set('views', 'views'); // set the views folder, current setting is default setting

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a'});

app.use(helmet());
app.use(compression());
app.use(morgan('combined', { stream: accessLogStream }));

/*
This body-parser module parses the JSON, buffer, 
string and URL encoded data submitted using HTTP POST request. 
*/
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('image'));  // handle multidata forms
app.use(express.static(path.join(__dirname, 'public'))); // set public folder as "public" to serve files like .css, etc
app.use('/images', express.static(path.join(__dirname, 'images'))); // set images folder as "public" to serve files like imgs etc and configure /images route
app.use(
    session({
        secret: 'my secret',
        resave: false,
        saveUninitialized: false,
        store: store
    })
);
app.use(flash());

app.use((req, res, next) => {   // add session 
    res.locals.isAuthenticated = req.session.isLoggedIn;
    next();
});

app.use((req, res, next) => {   // extract the user model
    if (!req.session.user) {
        return next();
    }
    User.findById(req.session.user._id)
        .then(user => {
            if (!user) {
                return next();
            }
            req.user = user;
            next();
        })
        .catch(err => {
            next(new Error(err));
        });
});

app.post('/create-order', isAuth, shopController.postOrder);

app.use(csrfProtection);    // will protect from csrfAttacks
app.use((req, res, next) => {   // add csrf token
    res.locals.csrfToken = req.csrfToken();
    next();
});

app.use('/admin', adminRoutes); // handle all routes starting /admin
app.use(shopRoutes); // handle routes starting with /, order matters depending on how the routes are handled
app.use(authRoutes);

app.get('/500', errorController.get500);

app.use(errorController.get404);

app.use((error, req, res, next) => {
    res.redirect('/500');
});

mongoose
    .connect(
        MONGODB_URI, {
            useNewUrlParser: true
        }
    )
    .then(result => {
        // https.createServer({key: privateKey, cert: certificate }, app).listen(process.env.PORT || 3000);
        app.listen(process.env.PORT || 3000);
    })
    .catch(err => console.log(err));
