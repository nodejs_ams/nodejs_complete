const path = require('path');

// this gives us the path to the file that is responsible that our app is running
module.exports = path.dirname(process.mainModule.filename);